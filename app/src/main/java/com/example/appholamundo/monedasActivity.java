package com.example.appholamundo;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import org.w3c.dom.Text;

public class monedasActivity extends AppCompatActivity {
    private EditText txtCantidad;
    private TextView txtResultado;
    private Spinner spinner;
    private Button btnCalcular, btnLimpiar,btnCerrar;
    private int pos =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_monedas);

        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtCantidad.getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Falto capturar", Toast.LENGTH_SHORT).show();
                }
                else {
                    float cantidad = Float.parseFloat(txtCantidad.getText().toString());
                    float resultado;
                    switch (pos){
                        case 0 : //Pesos a dolares Americanos
                            resultado = (cantidad/16.64f);
                            txtResultado.setText("Resultado: " + resultado + "USD");
                            break;
                        case 1 : //Pesos a Dolar canadiense
                            resultado = (cantidad/12.22f);
                            txtResultado.setText("Resultado: " + resultado + "CAD");
                            break;
                        case 2 : //Peso a Euro
                            resultado = (cantidad/18.12f);
                            txtResultado.setText("Resultado: " + resultado + "EUR");
                            break;
                        case 3 : //Peso a Libras
                            resultado = (cantidad/21.27f);
                            txtResultado.setText("Resultado: " + resultado + "GBP");
                            break;

                            default:
                            break;
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                txtResultado.setText("Resultado: ");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        spinner = (Spinner) findViewById(R.id.spnMoneda);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        //adaptar el Array a Spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.monedas));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);



    }
}