package com.example.appholamundo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class conversionGradosActivity extends AppCompatActivity {
    private TextView txtResultado;
    private EditText txtCantidad;
    private Button btnCalcular, btnCerrar, btnLimpiar;
    private RadioButton rdbCel, rdbFa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_conversion_grados);

        iniciarComponentes();
        //evento click calcular
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cantidadStr = txtCantidad.getText().toString();
                if (!cantidadStr.isEmpty()) {
                    double cantidad = Double.parseDouble(cantidadStr);
                    double resultado;
                    if (rdbCel.isChecked()) {
                        resultado = (cantidad * 9 / 5) + 32;
                        txtResultado.setText("Resultado: " + resultado + " °F");
                    } else if (rdbFa.isChecked()) {
                        resultado = (cantidad - 32) * 5 / 9;
                        txtResultado.setText("Resultado: " + resultado + " °C");
                    }
                else if (txtCantidad.getText().toString().matches("")) {
                    Toast.makeText(getApplicationContext(),"Capturar la cantidad", Toast.LENGTH_SHORT).show();
                }


                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                txtResultado.setText("");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        rdbCel = (RadioButton) findViewById(R.id.rdbCel);
        rdbFa = (RadioButton) findViewById(R.id.rdbFa);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }
}