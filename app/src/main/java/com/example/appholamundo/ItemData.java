package com.example.appholamundo;
//atributos
public class ItemData {
    private String txtCategoria, txtDescripcion;
    private int imageId;

    //constructores
    public ItemData() {
        this.txtCategoria = "";
        this.txtDescripcion = "";
        this.imageId = 0;
    }

    public ItemData(String txtCategoria, String txtDescripcion, int imageId) {
        this.txtCategoria = txtCategoria;
        this.txtDescripcion = txtDescripcion;
        this.imageId = imageId;
    }

    public ItemData(ItemData itemdata){
        this.txtCategoria = itemdata.txtCategoria;
        this.txtDescripcion = itemdata.txtDescripcion;
        this.imageId = itemdata.imageId;
    }

    //Encapsulado

    public String getTxtCategoria() {
        return txtCategoria;
    }

    public void setTxtCategoria(String txtCategoria) {
        this.txtCategoria = txtCategoria;
    }

    public String getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(String txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }


}
