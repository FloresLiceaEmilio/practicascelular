package com.example.appholamundo;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MenuActivity extends AppCompatActivity {

    private CardView crvPrimer, crvImc, crvConversion, crvMoneda, crvCotizacion, crvSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);





        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_menu);
        iniciarComponentes();

        //codificar los eventos click de las tarjetas
        crvPrimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

        crvImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this,ImcActivity.class);
                startActivity(intent);
            }
        });

        crvConversion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, conversionGradosActivity.class);
                startActivity(intent);
            }
        });

        crvMoneda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, monedasActivity.class);
                startActivity(intent);
            }
        });

        crvCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, cotizacionIngresoActivity.class);
                startActivity(intent);
            }
        });

        crvSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, SpinnerPersonalizado.class);
                startActivity(intent);
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){
        crvPrimer = (CardView) findViewById(R.id.crvHola);
        crvImc = (CardView) findViewById(R.id.crvIMC);
        crvMoneda = (CardView) findViewById(R.id.crvMoneda);
        crvConversion = (CardView) findViewById(R.id.crvConversion);
        crvCotizacion = (CardView) findViewById(R.id.crvCotizacion);
        crvSpinner = (CardView) findViewById(R.id.crvSpinner);
    }
}